/// <reference types="core-js" />
export declare class Deferred {
    promise: Promise<any>;
    resolve: Function;
    reject: Function;
    constructor();
}
