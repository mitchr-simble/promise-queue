"use strict";
var deferred_1 = require('./deferred');
var BehaviorSubject_1 = require('rxjs/BehaviorSubject');
var PromiseQueue = (function () {
    function PromiseQueue() {
        this.queue = [];
        this.isRunning = false;
        this.subject = new BehaviorSubject_1.BehaviorSubject({
            isRunning: this.isRunning,
            msg: ''
        });
    }
    PromiseQueue.prototype.log = function (message) {
        this.subject.next({
            isRunning: this.isRunning,
            msg: message
        });
    };
    PromiseQueue.prototype.add = function (fn) {
        var params = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            params[_i - 1] = arguments[_i];
        }
        var dfd = new deferred_1.Deferred();
        this.queue.push({
            fn: fn, params: params, dfd: dfd
        });
        this.log('Task pushed to Queue');
        if (!this.isRunning) {
            this.isRunning = true;
            this.log('Queue begun processing');
            this.run();
        }
        return dfd.promise;
    };
    PromiseQueue.prototype.run = function () {
        var _this = this;
        var task = this.queue[0];
        if (task) {
            this.log('Task Begun');
            var params = task.params;
            var fn = task.fn;
            fn.apply(null, params)
                .then(function (res) {
                _this.log('Task Resovled');
                task.dfd.resolve(res);
                _this.next();
            })
                .catch(function (err) {
                _this.log('Task Rejected');
                task.dfd.reject(err);
                _this.next();
            });
        }
        else {
            this.isRunning = false;
            this.log('Queue completed processing.');
        }
    };
    PromiseQueue.prototype.next = function () {
        this.queue.shift();
        this.run();
    };
    return PromiseQueue;
}());
exports.PromiseQueue = PromiseQueue;
