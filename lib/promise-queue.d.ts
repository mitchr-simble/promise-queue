/// <reference types="core-js" />
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
export declare class PromiseQueue {
    private queue;
    private isRunning;
    subject: BehaviorSubject<any>;
    constructor();
    private log(message);
    add(fn: Function, ...params: Array<any>): Promise<any>;
    private run();
    private next();
}
