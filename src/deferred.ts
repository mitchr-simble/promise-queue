/**
 * Simple class that creates a Native Promise that can be resolved/rejected outside of it's scope
 */
export class Deferred {
  public promise: Promise<any>;
  public resolve: Function;
  public reject: Function;

  constructor() {
    this.promise = new Promise((resolve, reject) => {
      this.resolve = resolve;
      this.reject = reject;
    });
  }
}
