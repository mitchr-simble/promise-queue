import { Deferred } from './deferred';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

export class PromiseQueue {
  private queue: Array<any> = [];
  private isRunning: boolean = false;
  public subject: BehaviorSubject<any>;

  constructor() {
    this.subject = new BehaviorSubject({
      isRunning: this.isRunning,
      msg: '',
    });
  }

  private log(message: string) {
    this.subject.next({
      isRunning: this.isRunning,
      msg : message,
    });
  }

  public add(fn: Function, ...params: Array<any>) {
    let dfd = new Deferred();
    this.queue.push({
      fn, params, dfd,
    });
    this.log('Task pushed to Queue');
    if (!this.isRunning) {
      this.isRunning = true;
      this.log('Queue begun processing');
      this.run();
    }
    return dfd.promise;
  }

  private run() {
    let task = this.queue[0];
    if (task) {
      this.log('Task Begun');
      let params = task.params;
      let fn = task.fn;
      fn.apply(null, params)
        .then((res: any) => {
          this.log('Task Resovled');
          task.dfd.resolve(res);
          this.next();
        })
        .catch((err: any) => {
          this.log('Task Rejected');
          task.dfd.reject(err);
          this.next();
        });
    } else {
      this.isRunning = false;
      this.log('Queue completed processing.');
    }
  }

  private next() {
    this.queue.shift();
    this.run();
  }
}
