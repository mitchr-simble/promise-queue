# Promise-Queue
This module creates a queue for Promises and calls them in series. Was originally conceived to prevent hitting the limit of max http connections within browsers when syncing multiple Pouch/Couch Databases.

The add() method is thenable and allows any number of parameters to be passed.

It uses rxjs to allow easy subscription of reporting events.

### Basic Usage

```ts
import { PromiseQueue } from './lib/index';

const pq = new PromiseQueue;

const testFn = (timeout, ...items) => {
  return new Promise((resolve, reject) => {
    setTimeout( () => {
      resolve(items);
    }, timeout);
  });
}

pq.add(testFn, 3000, 'test1')
.then( (items) => {
  console.log('');
  console.log(...items);
});

pq.add(testFn, 5000, 'test2', 'test3')
.then( (items) => {
  console.log('');
  console.log(...items);
});

```

### Subscribe to Observable

The subect() method emits data to enable easy reporting and status checks.
```ts
let subscription = pq.subject.subscribe(
  (x) => {
    console.log('Next: ', x);
  },
  (err) => {
    console.log('Error: ', err);
  },
  () => {
    console.log('completed');
  }
)
```

## TODO

- Write Tests
- Rename to allow publishing to npm
